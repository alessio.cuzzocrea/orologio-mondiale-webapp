    var ngapp = angular.module('orologioMondiale', ["ngRoute"]);
    ngapp.config(function ($routeProvider) {
        $routeProvider.when("/searchCities", {
            templateUrl: "partials/searchCities.html",
            controller: "SearchCitiesController",
            controllerAs: "searchCtrl"
        })
            .when("/favoritedCities", {
                templateUrl: "partials/favoritedCities.html",
                controller: "FavoritedCitiesController",
                controllerAs: "favCtrl"
            })
            .when("/convertHour", {
                templateUrl: "partials/convertHour.html",
                controller: "ConvertHourController",
                controllerAs: "cvtCtrl"
            })
            .when("/cityDetails", {
                templateUrl: "partials/cityDetails.html",
                controller: "CityDetailsController",
                controllerAs: "detailsCtrl"
            })
            .otherwise("/searchCities")

    });
    ngapp.factory('State', function () {
        return {
            searchCitiesState: {
                geoServicesDeafaultRequest : "https://secure.geonames.org/search?username=a.cuzzocrea&type=json&orderby=population&maxRows=10&cities=cities1000&lang=it&style=medium",
                geoServiceBaseUrl: "https://secure.geonames.org/search?",
                searchResults: [],
                geoServiceParams: {
                    username: "a.cuzzocrea",
                    type: "json",
                    cities: "cities1000",
                    maxRows: "10",
                    lang: "it",
                    searchlang: "it",
                    orderby: "relevance",
                    isNameRequired: "true",
                    name_startsWith: "",
                    style: "MEDIUM"
                }
            },
            fileParams: {
                fileName: "favoritedCities.json"
            },
            navBarParams: {
                tabIndex: 1,
                navTitles : {
                    "/searchCities": "Inserisci città",
                    "/favoritedCities": "Lista città",
                    "/convertHour": "Converti orario",
                    "/cityDetails": "Dettagli città"
                },
                // currentTitle: "Ricerca città",
            },
            errorOnTimezoneService : false,
            cityDetail: {
                selectedCity: {}
            }
        };
    });
    ngapp.service('CitiesManager', ["$filter", function ($filter) {
        this.favoritedCities = [];
        this.favoriteCity = function (city) {
            this.favoritedCities.push(city);
        };

        this.getFavoritedCities = function () {
            console.log("called!");
            console.log(this.favoritedCities);
            return this.favoritedCities;
        };
        this.unfavoriteCity = function (geoId) {
            var index = -1;
            index = this.favoritedCities.findIndex(function (city) {
                return city.geonameId == geoId;
            });
            this.favoritedCities.splice(index, 1);
        };
        this.contains = function (searchedId) {
            var found = [];
            found = $filter('filter')(this.favoritedCities, {geonameId: searchedId});
            if (found.length > 0)
                return true
            return false;
        };
        this.updateCity = function(city){
            var index = -1;
            index = this.favoritedCities.findIndex(function (element) {
                return element.geonameId == city.geo ;
            });
            this.favoritedCities[index] = city;

        };
    }]);
    // ngapp.run(function (State, CitiesManager, $location, $route, $rootScope) {
    //     document.addEventListener("deviceready", onDeviceReady, false);
    //     document.addEventListener("pause", onDevicePause, false);
    //     document.addEventListener("resume", onDeviceResume, false);
    //
    //     function onDeviceResume() {
    //         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    //
    //
    //             fs.root.getFile(State.fileParams.fileName, {create: false, exclusive: false}, function (fileEntry) {
    //                 fileEntry.file(function (file) {
    //                     var reader = new FileReader();
    //                     reader.onloadend = function () {
    //                         CitiesManager.favoritedCities = JSON.parse(this.result);
    //                         CitiesManager.favoritedCities = angular.copy(JSON.parse(this.result));
    //
    //                     };
    //                     reader.onerror = function () {
    //                         console.log(err)
    //                     };
    //                     reader.readAsText(file);
    //                 });
    //             });
    //         });
    //     }
    //
    //     function onDeviceReady() {
    //         //is first run?
    //         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    //
    //             fs.root.getFile(State.fileParams.fileName, {create: false, exclusive: false}, function (fileEntry) {
    //                 //read the file, check if it has been written
    //
    //                 fileEntry.file(function (file) {
    //                     var reader = new FileReader();
    //                     reader.onloadend = function () {
    //                         try {
    //                             CitiesManager.favoritedCities = angular.copy(JSON.parse(this.result));
    //                             $location.replace();
    //                             $location.url("/favoritedCities");
    //                             State.navBarParams.tabIndex = 2;
    //                             $route.reload();
    //                         }
    //                         catch (err) {
    //                             console.log(err);
    //                             $location.replace();
    //                             $location.url("/searchCities");
    //                             State.navBarParams.tabIndex = 1;
    //                             $route.reload();
    //                         }
    //                     };
    //                     reader.onerror = function () {
    //                         console.log("some error was thrown while reading the file");
    //                         console.log(this);
    //                     };
    //                     reader.readAsText(file);
    //                 }, function (error) {
    //                     console.log(this);
    //                     console.log(error)
    //                 });
    //
    //             }, function (err) {
    //                 //the file does not exist, create it and redirect to searchCities
    //                 fs.root.getFile(State.fileParams.fileName, {create: true, exclusive: false}, function (fileEntry) {
    //                     $location.replace();
    //                     $location.url("/searchCities");
    //                     $route.reload();
    //                 });
    //                 console.log(err)
    //             });
    //
    //         }, function (err) {
    //             console.log(err)
    //         });
    //     }
    //
    //     function onDevicePause() {
    //         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    //             fs.root.getFile(State.fileParams.fileName, {create: true, exclusive: false}, function (fileEntry) {
    //                 fileEntry.createWriter(function (fileWriter) {
    //                     fileWriter.onwriteend = function () {
    //                         fileEntry.file(function (file) {
    //                             fileWriter.onwriteend = function () {
    //                             };
    //                             fileWriter.write(JSON.stringify(CitiesManager.getFavoritedCities()), {type: "application/json"});
    //                         })
    //                     };
    //                     //svuota il file e scrivi le nuoove città
    //                     fileWriter.truncate(0);
    //                 });
    //             });
    //         });
    //     }
    //
    //
    // });



