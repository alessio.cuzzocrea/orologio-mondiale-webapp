/**
 * Created by Alessio on 20/02/2017.
 */
    ngapp.controller('SearchCitiesController', ['$scope', '$http', "$httpParamSerializer", 'CitiesManager', 'State', function ($scope, $http, $httpParamSerializer, CitiesManager, State) {
        this.searchResults = [];
        this.geoServiceParams = State.searchCitiesState.geoServiceParams;
        this.geoServiceParams.name_startsWith = "";
        this.timezoneServiceUrl = "https://secure.geonames.org/timezoneJSON?";
        this.timezoneServiceParams = {
            username: "a.cuzzocrea",
            type: "json",
            lat: "",
            lng: ""
        };
        //setup first cities
        $http({
            url: State.searchCitiesState.geoServicesDeafaultRequest,
            method: 'GET'
        }).then(function successCallback(response) {
            $scope.searchCtrl.searchResults = response.data.geonames;
            //State.searchCitiesState.searchResults = response.data.geonames;
        });

        this.query = function () {
            var searchedCity = angular.copy(this.geoServiceParams.name_startsWith);
            $http({
                url: State.searchCitiesState.geoServiceBaseUrl + $httpParamSerializer(this.geoServiceParams),
                method: 'GET'
            }).then(function successCallback(response) {
                if($("#search-box").val() === searchedCity)
                    $scope.searchCtrl.searchResults = response.data.geonames;
            });
        };
        this.favoriteCity = function ($event, city) {
            if (CitiesManager.contains(city.geonameId))
                CitiesManager.unfavoriteCity(city.geonameId);
            else {
                this.timezoneServiceParams.lat = city.lat;
                this.timezoneServiceParams.lng = city.lng;
                $http({
                    url: this.timezoneServiceUrl + $httpParamSerializer(this.timezoneServiceParams),
                    method: "GET"
                }).then(function successCallback(response) {
                    city.timezone = response.data;
                    CitiesManager.favoriteCity(city);
                });
            }
        };
        this.isFavorited = function (geonameId) {
            return CitiesManager.contains(geonameId);
        }
    }]);
    ngapp.controller('FavoritedCitiesController', ["$location", "$scope","$http", "$httpParamSerializer", "CitiesManager", "State", function ($location, $scope, $http,$httpParamSerializer, CitiesManager, State) {

        this.favoritedCities = angular.copy(CitiesManager.getFavoritedCities());
        this.cityDetailDiv = "cityDetailDiv";
        this.errorOnTimezoneService = false;
        this.timezoneServiceUrl = "http://secure.geonames.org/timezoneJSON?";
        this.timezoneServiceParams = {
            username: "a.cuzzocrea",
            type: "json",
            lat: "",
            lng: ""
        };
        //scroll the cityDetailDiv
        $(window).scroll(function(){
            $scrollingDiv = $("#cityDetailDiv");
            $scrollingDiv
                .stop()
                .animate({"marginTop": ($(window).scrollTop() )}, "slow" );
        });

        try {
            CitiesManager.contains(State.cityDetail.selectedCity.geonameId) ? noop() : angular.copy({}, State.cityDetail.selectedCity);
        }
        catch (e) {
            console.log("selected city not set or has been deleted");
        }
        this.viewDetails = function ($event, city) {
            this.timezoneServiceParams.lat = city.timezone.lat;
            this.timezoneServiceParams.lng = city.timezone.lng;
            $http({
                url: this.timezoneServiceUrl + $httpParamSerializer(this.timezoneServiceParams),
                method: "GET"
            }).then(angular.bind(this, function successCallback(response) {
                    city.timezone = response.data;
                    city.timezone.sunset = moment(city.timezone.sunset).format();
                    city.timezone.sunrise = moment(city.timezone.sunrise).format();
                    city.errorOnTimezoneService = false;
                    this.selectedCity = angular.copy(city);
                    CitiesManager.updateCity(city);
                    angular.copy(city, State.cityDetail.selectedCity);
                    if ($("#" + this.cityDetailDiv).is(":hidden")) {
                        $location.url("/cityDetails");
                    }
                }),
                angular.bind(this,function errorCallback(response){
                    city.timezone.sunset = moment(city.timezone.sunset).format();
                    city.timezone.sunrise = moment(city.timezone.sunrise).format();
                    city.errorOnTimezoneService = true;
                    this.selectedCity = angular.copy(city);
                    angular.copy(city, State.cityDetail.selectedCity);
                    if ($("#" + this.cityDetailDiv).is(":hidden")) {
                        $location.url("/cityDetails");
                    }
                }));
        };
        this.removeCity = function (city) {
            CitiesManager.unfavoriteCity(city.geonameId);
            this.favoritedCities = angular.copy(CitiesManager.getFavoritedCities());
        }
    }]);
    ngapp.controller('ConvertHourController', ["CitiesManager", function (CitiesManager) {
        this.favoritedCities = angular.copy(CitiesManager.favoritedCities);
        this.fromCity = {};
        this.toCity = {};
        this.timeToConvert = "";
        this.date = null;
        this.time = "";


        this.timeConverted = "";
        this.convert = function () {
            //iso formatting without timezone

            try {
                var tmpDate = moment.tz(this.date.format("YYYY-MM-DDTHH:mm:ss"), this.fromCity.timezone.timezoneId);
                if (tmpDate.isValid()) {
                    var convertedTime = moment.tz(tmpDate, this.fromCity.timezone.timezoneId);
                    this.timeConverted = convertedTime.clone().tz(this.toCity.timezone.timezoneId).format("LLLL");
                }
            }
            catch (e) {
                console.log("city not set")
            }
        };
        //onCHange si può eliminare, chiamare direttamente convert
        this.onChange = function () {
            this.convert();
        };
    }]);
    ngapp.controller('CityDetailsController', ["State", function (State) {
        this.selectedCity = State.cityDetail.selectedCity;
        // this.errorOnTimezoneService = State.errorOnTimezoneService;
    }]);
    ngapp.controller('ClockController', function ($scope, $timeout) {
        this.tickInterval = 1000;

        if (!$scope.timezone) {
            $scope.timezone = moment.tz.guess()
        }
        this.time = moment().tz($scope.timezone).format();
        this.offset = moment().tz($scope.timezone).format("Z");

        this.tick = function () {
            this.time = moment().tz($scope.timezone).format();
            this.offset = moment().tz($scope.timezone).format("Z");
            $timeout(angular.bind(this, this.tick), this.tickInterval);

        };
        $scope.$watch("timezone", function (newValue, oldValue, scope) {
            scope.clockCtrl.tick();
        });


    });

