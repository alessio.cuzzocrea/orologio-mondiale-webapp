Orolongio mondiale scritta con AngularJS.
L'app permette di tenere traccia degli orari di diverse città del mondo e di convertire  i diversi fuso orari.

La webapp è raggiungibile al seguente link:
[text](http://alessio.cuzzocrea.gitlab.io/orologio-mondiale-webapp/#/searchCities)